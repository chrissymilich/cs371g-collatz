.DEFAULT_GOAL := all
MAKEFLAGS     += --no-builtin-rules
SHELL         := bash

ASTYLE        := astyle
CHECKTESTDATA := checktestdata
CPPCHECK      := cppcheck
DOXYGEN       := doxygen

ifeq ($(shell uname -s), Darwin)
    BOOST    := /usr/local/include/boost
    CXX      := g++-11
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -I/usr/local/include -Wall -Wextra
    GCOV     := gcov-11
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -lgtest -lgtest_main
    LIB      := /usr/local/lib
    VALGRIND :=
else ifeq ($(shell uname -p), unknown)
    BOOST    := /usr/include/boost
    CXX      := g++
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    GCOV     := gcov
    GTEST    := /usr/src/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
    LIB      := /usr/lib
    VALGRIND := valgrind
else
    BOOST    := /usr/include/boost
    CXX      := g++-9
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    GCOV     := gcov-9
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
    LIB      := /usr/local/lib
    VALGRIND := valgrind
endif

# run docker
docker:
	docker run -it -v $(PWD):/usr/gcc -w /usr/gcc gpdowning/gcc

# get git config
config:
	git config -l

# get git log
Collatz.log:
	git log > Collatz.log

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status

# download files from the Collatz code repo
pull:
	make clean
	@echo
	git pull
	git status

# upload files to the Collatz code repo
push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Collatz.cpp
	git add Collatz.hpp
	-git add Collatz.log
	-git add html
	git add makefile
	git add README.md
	git add RunCollatz.cpp
	git add RunCollatz.ctd
	git add TestCollatz.cpp
	git commit -m "another commit"
	git push
	git status

# compile run harness
RunCollatz: Collatz.hpp Collatz.cpp RunCollatz.cpp
	-$(CPPCHECK) Collatz.cpp
	-$(CPPCHECK) RunCollatz.cpp
	$(CXX) $(CXXFLAGS) Collatz.cpp RunCollatz.cpp -o RunCollatz

# compile test harness
# can add -DNDEBUG here
TestCollatz: Collatz.hpp Collatz.cpp TestCollatz.cpp
	-$(CPPCHECK) Collatz.cpp
	-$(CPPCHECK) TestCollatz.cpp
	$(CXX) $(CXXFLAGS) Collatz.cpp TestCollatz.cpp -o TestCollatz $(LDFLAGS)

# run/test files, compile with make all
FILES :=                                  \
    RunCollatz                            \
    TestCollatz

# compile all
all: $(FILES)

# execute test harness
test: TestCollatz
	$(VALGRIND) ./TestCollatz
ifeq ($(shell uname -s), Darwin)
	$(GCOV) TestCollatz-Collatz.cpp | grep -B 2 "cpp.gcov"
else ifeq ($(shell uname -p), unknown)
	$(GCOV) TestCollatz-Collatz.cpp | grep -B 2 "cpp.gcov"
else
	$(GCOV) Collatz.cpp | grep -B 2 "cpp.gcov"
endif

# test files in the Collatz test repo
T_FILES := `ls collatz-tests/*.in`

# check integrity of test files in the Collatz test repo
ctd-check:
	-for v in $(T_FILES); do echo $(CHECKTESTDATA) RunCollatz.ctd $$v; $(CHECKTESTDATA) RunCollatz.ctd $$v; done

# generate a random input file
ctd-generate:
	$(CHECKTESTDATA) -g RunCollatz.ctd > RunCollatz.gen

# clone the Collatz test repo
collatz-tests:
	git clone https://gitlab.com/gpdowning/cs371g-collatz-tests.git collatz-tests

# execute run harness against a test in Collatz test repo and diff with expected output
collatz-tests/%: RunCollatz
	$(CHECKTESTDATA) RunCollatz.ctd $@.in
	./RunCollatz < $@.in > RunCollatz.tmp
	-diff RunCollatz.tmp $@.out

# execute run harness against all tests in Collatz test repo and diff with expected output
run: collatz-tests RunCollatz
	-for v in $(T_FILES); do make $${v/.in/}; done

# auto format the code
format:
	$(ASTYLE) Collatz.cpp
	$(ASTYLE) Collatz.hpp
	$(ASTYLE) RunCollatz.cpp
	$(ASTYLE) TestCollatz.cpp

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATIC  to YES
# create Doxfile
Doxyfile:
	$(DOXYGEN) -g

# create html directory
html: Doxyfile
	$(DOXYGEN) Doxyfile

# check files, check their existence with make check
C_FILES :=                           \
    .gitignore                       \
    .gitlab-ci.yml                   \
    Collatz.log                      \
    html

# check the existence of check files
check: $(C_FILES)

# remove executables and temporary files
clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.gen
	rm -f *.plist
	rm -f *.tmp
	rm -f RunCollatz
	rm -f TestCollatz

# remove executables, temporary files, and generated files
scrub:
	make clean
	rm -f  Collatz.log
	rm -f  Doxyfile
	rm -rf collatz-tests
	rm -rf html
	rm -rf latex

# output versions of all tools
versions:
	@echo "% which $(ASTYLE)"
	@which $(ASTYLE)
	@echo
	@echo "% $(ASTYLE) --version"
	@$(ASTYLE) --version

	@echo
	@echo "% which $(CHECKTESTDATA)"
	@which $(CHECKTESTDATA)
	@echo
	@echo "% $(CHECKTESTDATA) --version"
	@$(CHECKTESTDATA) --version

	@echo
	@echo "% which cmake"
	@which cmake
	@echo
	@echo "% cmake --version"
	@cmake --version

	@echo
	@echo "% which $(CPPCHECK)"
	@which $(CPPCHECK)
	@echo
	@echo "% $(CPPCHECK) --version"
	@$(CPPCHECK) --version

	@echo
	@echo "% which $(DOXYGEN)"
	@which $(DOXYGEN)
	@echo
	@echo "% $(DOXYGEN) --version"
	@$(DOXYGEN) --version

	@echo
	@echo "% which $(CXX)"
	@which $(CXX)
	@echo
	@echo "% $(CXX) --version"
	@$(CXX) --version

	@echo
	@echo "% which $(GCOV)"
	@which $(GCOV)
	@echo
	@echo "% $(GCOV) --version"
	@$(GCOV) --version

	@echo
	@echo "% which git"
	@which git
	@echo
	@echo "% git --version"
	@git --version

	@echo
	@echo "% which make"
	@which make
	@echo
	@echo "% make --version"
	@make --version

ifneq ($(shell uname -s), Darwin)
	@echo
	@echo "% which $(VALGRIND)"
	@which $(VALGRIND)
	@echo
	@echo "% $(VALGRIND) --version"
	@$(VALGRIND) --version
endif

	@echo "% which vim"
	@which vim
	@echo
	@echo "% vim --version"
	@vim --version

	@echo
	@echo "% grep \"#define BOOST_LIB_VERSION \" $(BOOST)/version.hpp"
	@grep "#define BOOST_LIB_VERSION " $(BOOST)/version.hpp

	@echo
	@echo "% grep \"set(GOOGLETEST_VERSION\" $(GTEST)/CMakeLists.txt"
	@grep "set(GOOGLETEST_VERSION" $(GTEST)/CMakeLists.txt
	@echo
	@echo "% ls -al $(LIB)/libgtest*.a"
	@ls -al $(LIB)/libgtest*.a
