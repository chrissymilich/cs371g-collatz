// ---------------
// TestCollatz.cpp
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----------------
// max_cycle_length
// ----------------

TEST(CollatzFixture, max_cycle_length_0) {
    ASSERT_EQ(max_cycle_length(1, 10), 20u);}

TEST(CollatzFixture, max_cycle_length_1) {
    ASSERT_EQ(max_cycle_length(100, 200), 125u);}

TEST(CollatzFixture, max_cycle_length_2) {
    ASSERT_EQ(max_cycle_length(201, 210), 89u);}

TEST(CollatzFixture, max_cycle_length_3) {
    ASSERT_EQ(max_cycle_length(900, 1000), 174u);}

TEST(CollatzFixture, max_cycle_length_4) {
    ASSERT_EQ(max_cycle_length(1, 100000), 351u);}

TEST(CollatzFixture, max_cycle_length_5) {
    ASSERT_EQ(max_cycle_length(100001, 200000), 383u);}

TEST(CollatzFixture, max_cycle_length_6) {
    ASSERT_EQ(max_cycle_length(200001, 300000), 443u);}

TEST(CollatzFixture, max_cycle_length_7) {
    ASSERT_EQ(max_cycle_length(300001, 400000), 441u);}

TEST(CollatzFixture, max_cycle_length_8) {
    ASSERT_EQ(max_cycle_length(400001, 500000), 449u);}

TEST(CollatzFixture, max_cycle_length_9) {
    ASSERT_EQ(max_cycle_length(500001, 600000), 470u);}

TEST(CollatzFixture, max_cycle_length_10) {
    ASSERT_EQ(max_cycle_length(600001, 700000), 509u);}

TEST(CollatzFixture, max_cycle_length_11) {
    ASSERT_EQ(max_cycle_length(700001, 800000), 504u);}

TEST(CollatzFixture, max_cycle_length_12) {
    ASSERT_EQ(max_cycle_length(800001, 900000), 525u);}

TEST(CollatzFixture, max_cycle_length_13) {
    ASSERT_EQ(max_cycle_length(900001, 999999), 507u);}

TEST(CollatzFixture, max_cycle_length_14) {
    ASSERT_EQ(max_cycle_length(999998, 999999), 259u);}

TEST(CollatzFixture, max_cycle_length_15) {
    ASSERT_EQ(max_cycle_length(1, 80), 116u);}

TEST(CollatzFixture, max_cycle_length_16) {
    ASSERT_EQ(max_cycle_length(1, 85), 116u);}

TEST(CollatzFixture, max_cycle_length_17) {
    ASSERT_EQ(max_cycle_length(1, 90), 116u);}

TEST(CollatzFixture, max_cycle_length_18) {
    ASSERT_EQ(max_cycle_length(400, 500), 142u);}

TEST(CollatzFixture, max_cycle_length_19) { 
    ASSERT_EQ(max_cycle_length(1000, 2000), 182u);} 

TEST(CollatzFixture, max_cycle_length_20) {
    ASSERT_EQ(max_cycle_length(500, 2500), 209u);}

TEST(CollatzFixture, max_cycle_length_21) {
    ASSERT_EQ(max_cycle_length(999999, 999998), 259u);}