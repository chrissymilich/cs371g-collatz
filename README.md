# CS371g: Generic Programming Collatz Repo

* Name: Christine Milich

* EID: crm4792

* GitLab ID: chrissymilich

* HackerRank ID: chrissymilich

* Git SHA: fedca172850e89c6590fed47c07b8bd2ee038276

* GitLab Pipelines: https://gitlab.com/chrissymilich/cs371g-collatz/-/pipelines

* Estimated completion time: 6 hours

* Actual completion time: 10 hours

* Comments: I did this project in CS373 in python so I was able to work from a lot of that code to help me.
