// -----------
// Collatz.hpp
// -----------

#ifndef Collatz_hpp
#define Collatz_hpp

// ----------------
// max_cycle_length
// ----------------

/**
 * @param two positive ints
 * @return one positive int
 */
unsigned max_cycle_length (unsigned i, unsigned j);

#endif // Collatz_hpp
